#include <memory>
#include <csignal>
#include <stdexcept>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <thread>
#include <unistd.h>
#include <vector>

#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>

#include <wait.h>
#include <iostream>

#include "fmt/format.h"

// https://quuxplusone.github.io/blog/2020/01/24/openssl-part-1/
// https://quuxplusone.github.io/blog/2020/01/25/openssl-part-2/


namespace my {
    template<class T>
    struct DeleterOf;

    template<>
    struct DeleterOf<SSL_CTX> {
        void operator()(SSL_CTX *p) { SSL_CTX_free(p); }
    };

    template<>
    struct DeleterOf<SSL> {
        void operator()(SSL *p) { SSL_free(p); }
    };

    template<>
    struct DeleterOf<BIO> {
        void operator()(BIO *p) { BIO_free_all(p); }
    };

    template<>
    struct DeleterOf<BIO_METHOD> {
        void operator()(BIO_METHOD *p) { BIO_meth_free(p); }
    };

    template<class OpenSSLType>
    using UniquePtr = std::unique_ptr<OpenSSLType, my::DeleterOf<OpenSSLType>>;

    class StringBIO {
        std::string str_;
        my::UniquePtr<BIO_METHOD> methods_;
        my::UniquePtr<BIO> bio_;
    public:
        StringBIO(StringBIO &&) = delete;

        StringBIO &operator=(StringBIO &&) = delete;

        explicit StringBIO() {
            methods_.reset(BIO_meth_new(BIO_TYPE_SOURCE_SINK, "StringBIO"));
            if (!methods_) throw std::runtime_error("StringBIO: error in BIO_meth_new");

            BIO_meth_set_write(methods_.get(), [](BIO *bio, const char *data, int len) -> int {
                auto str = reinterpret_cast<std::string *>(BIO_get_data(bio));
                str->append(data, len);
                return len;
            });

            bio_.reset(BIO_new(methods_.get()));
            if (!bio_) throw std::runtime_error("StringBIO: error in BIO_new");

            BIO_set_data(bio_.get(), &str_);
            BIO_set_init(bio_.get(), 1);
        }

        BIO *bio() { return bio_.get(); }

        std::string str() &&{ return std::move(str_); };
    };

    [[noreturn]] void print_errors_and_exit(const char *message) {
        fmt::print(stderr, "{}\n", message);
        ERR_print_errors_fp(stderr);
        exit(1);
    }


    [[noreturn]] void print_errors_and_throw(const char *message) {
        my::StringBIO bio;
        ERR_print_errors(bio.bio());
        throw std::runtime_error(std::string(message) + "\n" + std::move(bio).str());
    }

    void send_http_request(BIO *bio, const std::string &line, const std::string &host) {
        std::string request = line + "\r\n";
        request += "Host: " + host + "\r\n";
        request += "\r\n";

        BIO_write(bio, request.data(), request.size());
        BIO_flush(bio);
    }

    void send_http_response(BIO *bio, const std::string &body) {
        std::string response = "HTTP/1.1 200 OK\r\n";
        response += "Content-Length: " + std::to_string(body.size()) + "\r\n";
        response += "\r\n";
        BIO_write(bio, response.data(), response.size());
        BIO_write(bio, body.data(), body.size());
        BIO_flush(bio);

    }

    std::string receive_some_data(BIO *bio) {
        char buffer[1024];
        int len = BIO_read(bio, buffer, sizeof(buffer));

        if (len < 0)
            my::print_errors_and_throw("Error in BIO_read");
        else if (len > 0)
            return std::string(buffer, len);
        else if (BIO_should_retry(bio))
            return receive_some_data(bio);
        else
            my::print_errors_and_throw("empty BIO_read");
    }

    std::vector<std::string> split_headers(const std::string &text) {
        std::vector<std::string> lines;
        const char *start = text.c_str();
        while (const char *end = strstr(start, "\r\n")) {
            lines.emplace_back(std::string(start, end));
            start = end + 2;
        }
        return lines;
    }

    std::string receive_http_message(BIO *bio) {
        std::string headers = my::receive_some_data(bio);
        char *end_of_headers = strstr(&headers[0], "\r\n\r\n");
        while (end_of_headers == nullptr) {
            headers += my::receive_some_data(bio);
            end_of_headers = strstr(&headers[0], "\r\n\r\n");
        }
        std::string body = std::string(end_of_headers + 4, &headers[headers.size()]);

        headers.resize((end_of_headers + 2 - &headers[0]));
        size_t content_length = 0;
        for (const std::string &line : my::split_headers(headers)) {
            if (const char *colon = strchr(line.c_str(), ':')) {
                auto header_name = std::string(&line[0], colon);
                if (header_name == "Content-Length")
                    content_length = std::stoul(colon + 1);
            }
        }
        while (body.size() < content_length)
            body += my::receive_some_data(bio);
        return headers + "\r\n" + body;
    }

    my::UniquePtr<BIO> operator|(my::UniquePtr<BIO> lower, my::UniquePtr<BIO> upper) {
        BIO_push(upper.get(), lower.release());
        return upper;
    }

    my::UniquePtr<BIO> accept_new_tcp_connection(BIO *accept_bio) {
        if (BIO_do_accept(accept_bio) <= 0) return nullptr;

        return my::UniquePtr<BIO>(BIO_pop(accept_bio));
    }

    SSL *get_ssl(BIO *bio) {
        SSL *ssl = nullptr;
        BIO_get_ssl(bio, &ssl);
        if (!ssl)
            my::print_errors_and_exit("Error in BIO_get_ssl");
        return ssl;
    }

    void verify_the_certificate(SSL *ssl, const std::string &expected_hostname)
    {
        long err = SSL_get_verify_result(ssl);
        if (err != X509_V_OK)
        {
            const char* message = X509_verify_cert_error_string(err);
            fmt::print(stderr, "Certificate verification error: {} ({})\n", message, err);
            exit(1);
        }
        X509 *cert = SSL_get_peer_certificate(ssl);
        if ( !cert )
        {
            fmt::print(stderr, "No certificate was presented by the server\n");
            exit(1);
        }

    }


}

namespace {
    [[noreturn]] void startHttpServerAndExit() {

        auto accept_bio = my::UniquePtr<BIO>(BIO_new_accept("8080"));
        if (BIO_do_accept(accept_bio.get()) <= 0)
            my::print_errors_and_exit("Error in BIO_do_accept (Binding to port 8080");

        static auto shutdown_the_socket = [fd = BIO_get_fd(accept_bio.get(), nullptr)]() {
            close(fd);
        };

        signal(SIGINT, [](int) { shutdown_the_socket(); });

        while (auto bio = my::accept_new_tcp_connection(accept_bio.get())) {
            try {
                std::string request = my::receive_http_message(bio.get());

                fmt::print("Got Request:\n");
                fmt::print("{}\n", request);
                my::send_http_response(bio.get(), "okay, cool\n");
            }
            catch (const std::exception &e) {
                fmt::print("Worker exited with exception: \n{}\n", e.what());
            }

        }
        fmt::print("\nClean exit!\n");

        exit(0);

    }

    [[noreturn]] void sendHttpRequestAndExit() {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(200ms);

        auto bio = my::UniquePtr<BIO>(BIO_new_connect("localhost:8080"));
        if (!bio) my::print_errors_and_exit("Error in BIO_new_connect");

        if (BIO_do_connect(bio.get()) <= 0) my::print_errors_and_exit("Error in BIO_do_connect");

        my::send_http_request(bio.get(), "GET / HTTP/1.1", "localhost");
        std::string response = my::receive_http_message(bio.get());
        fmt::print("{}", response);

        exit(0);

    }

    [[noreturn]] void startHttpsServerAndExit() {

        auto ctx = my::UniquePtr<SSL_CTX>(SSL_CTX_new(TLS_method()));
        SSL_CTX_set_min_proto_version(ctx.get(), TLS1_2_VERSION);

        if (SSL_CTX_use_certificate_file(ctx.get(), "../../server-certificate.pem", SSL_FILETYPE_PEM) <= 0) {
            my::print_errors_and_exit("Error loading server certificate");
        }

        if (SSL_CTX_use_PrivateKey_file(ctx.get(), "../../server-private-key.pem", SSL_FILETYPE_PEM) <= 0) {
            my::print_errors_and_exit("Error loading server private key ");
        }

        auto accept_bio = my::UniquePtr<BIO>(BIO_new_accept("8080"));
        if (BIO_do_accept(accept_bio.get()) <= 0)
            my::print_errors_and_exit("Error in BIO_do_accept (Binding to port 8080)");

        static auto shutdown_the_socket = [fd = BIO_get_fd(accept_bio.get(), nullptr)]() {
            close(fd);
        };

        signal(SIGINT, [](int) { shutdown_the_socket(); });

        while (auto bio = my::accept_new_tcp_connection(accept_bio.get())) {
            bio = std::move(bio) | my::UniquePtr<BIO>(BIO_new_ssl(ctx.get(), 0));
            try {
                std::string request = my::receive_http_message(bio.get());
                printf("Got request:\n");
                printf("%s\n", request.c_str());
                my::send_http_response(bio.get(), "okay cool\n");
            } catch (const std::exception &ex) {
                printf("Worker excited with exception:\n%s\n", ex.what());
            }


        }
        fmt::print("\nClean exit!\n");

        exit(0);

    }


    [[noreturn]] void sendHttpsRequestAndExit() {
        auto ctx = my::UniquePtr<SSL_CTX>(SSL_CTX_new(TLS_client_method()));

        if (SSL_CTX_set_default_verify_paths(ctx.get()) != 1)
            my::print_errors_and_exit("Error loading trust store");

        fmt::print("Default trust store is: {}", X509_get_default_cert_file());

        auto bio = my::UniquePtr<BIO>(BIO_new_connect("duckduckgo.com:443"));
        if (!bio)
            my::print_errors_and_exit("Error in BIO_new_connect");

        if (BIO_do_connect(bio.get()) <= 0)
            my::print_errors_and_exit("Error in BIO_do_connect");

        auto ssl_bio = std::move(bio) | my::UniquePtr<BIO>(BIO_new_ssl(ctx.get(), 1));

        SSL_set_tlsext_host_name(my::get_ssl(ssl_bio.get()), "duckduckgo.com");
        SSL_set1_host(my::get_ssl(ssl_bio.get()), "duckduckgo.com");

        if (BIO_do_handshake(ssl_bio.get()) <= 0)
            my::print_errors_and_exit("Error in TLS handshake");

        my::verify_the_certificate(my::get_ssl(ssl_bio.get()), "duckduckgo.com");

        my::send_http_request(ssl_bio.get(), "GET / HTTP/1.1", "duckduckgo.com");
        std::string response = my::receive_http_message(ssl_bio.get());
        fmt::print("{}", response.c_str());


        exit(0);
    }

    std::pair<int, std::string> getInput()
    {
        std::string userInput;
        std::getline(std::cin, userInput);

        if ( userInput.size() != 1 ) return {-1, userInput};

        int iUserInput = -1;

        try {
            iUserInput = std::stoi(std::forward<std::string>(userInput));
        } catch (std::exception& e) {
            return {-1, userInput};
        }
        return {iUserInput, userInput};

    }

}


int main()
{


    bool keepRunning = true;
    while ( keepRunning )
    {
        fmt::print("Select option:\n");
        fmt::print("  [0] Start HTTP Server.\n");
        fmt::print("  [1] Send HTTP Request.\n");
        fmt::print("  [2] Start HTTPS Server.\n");
        fmt::print("  [3] Send HTTPs Request\n");
        fmt::print("  [4] Exit\n");


        auto&& [ iInput, strInput ] = getInput();

        int pid = 0;
        switch( iInput )
        {
            case 0:
                pid = fork();
                if ( pid == 0 ) startHttpServerAndExit();
                break;
            case 1:
                pid = fork();
                if ( pid == 0 ) sendHttpRequestAndExit();
                break;
            case 2:
                pid = fork();
                if ( pid == 0 ) startHttpsServerAndExit();
                break;
            case 3:
                pid = fork();
                if ( pid == 0 ) sendHttpsRequestAndExit();
                break;
            case 4:
                keepRunning = false;
                break;
            default:
                fmt::print(stderr, "Error: \"{}\" is not a valid input!\n", strInput);
        }

    }

}